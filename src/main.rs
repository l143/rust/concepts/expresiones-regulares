extern crate regex;

use std::io;
use regex::Regex;

fn read_input(instructions: &str) -> String {
    println!("{}", instructions);
    let mut value =  String::new();

    match io::stdin().read_line(&mut value) {
        Ok(_) => return value,
        _ => panic!("Error leyendo valor de la terminal")
    }
}

fn check_expr(values: Vec<&str>, expr: &str) {
    let regular_expression = match Regex::new(expr) {
        Ok(r) => r,
        Err(e) => panic!(e)
    };

    for value in values.iter() {
        print!("Evaluando el dato: {} -> ", &value);
        if regular_expression.is_match(&value) {
            println!("ok 👻!");
        }
        else {
            println!("Opps 💩!");
        } 
    }
}

fn main() {
    println!("\nValidando expresión de teléfono.");
    let numbers = vec![
        "5540128869",
        "525540128869",
        "+5255401288",
        "+5255401288690",
        "+525540128869; delete *;",
        "+525540128869"
    ];
    let tel_expr = r"^\+52\d{10}$";
    check_expr(numbers, &tel_expr);

    println!("\nValidando expresión de email.");
    let emails = vec![
        "soporte.food@mx.didiglobal.com",
        "everardo.ipn@gmail.com",
        "everardo.sanchezh@sat.gob.mx",
        "rockfest_10@hotmail.com",
        "somefla@kde.com",
        "someflakde.com",
        "some@ol"
    ];
    check_expr(emails, r"^[\w\._]+@[\w\.\-]{2,}\.\w{2,}$")

}
